package main

import (
	"fmt"
	"strconv"
)

/* Define person struct */
type Person struct {
	/* Basic */
	// firstName string
	// lastName  string
	// city      string
	// gender    string
	// age       int

	/* Alternate */
	firstName, lastName, city, gender string
	age                               int
}

/* Greeting method (value reciever) */
func (person Person) greet() string {
	return "Hello, my name is " + person.firstName + " " +
		person.lastName + " and I'm " + strconv.Itoa(person.age)
}

/* hasBirtday method (pointer reciever) !change something */
func (person *Person) hasBirtday() {
	person.age++
}

/* getMarried (pointer reciever) */
func (person *Person) getMarried(spouseLastName string) {
	if person.gender == "M" {
		return
	} else {
		person.lastName = spouseLastName
	}
}

func main() {
	/* Init person using struct */
	person1 := Person{
		firstName: "Borja",
		lastName:  "Gete",
		city:      "Madrid",
		gender:    "M",
		age:       30,
	}

	/* Alternative */
	// person1 := Person{"Borja", "Gete", "Madrid", "M", 30}

	// fmt.Println(person1)
	fmt.Println(person1.greet())
	fmt.Println(person1.firstName)
	// person1.age++
	person1.hasBirtday()
	fmt.Println(person1.greet())

	person2 := Person{"Samantha", "Black", "Boston", "F", 25}
	fmt.Println(person2.greet())
	person2.getMarried("Williams")
	fmt.Println(person2.greet())
	fmt.Println(person2.greet())
	person1.getMarried("Green")
	fmt.Println(person1.greet())

}

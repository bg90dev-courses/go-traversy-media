package main

import "fmt"

var name = "Borja"

// name := "Borja" // not possible declaration outside function
func main() {
	// MAIN TYPES
	// string
	// bool
	// int
	// int int8 int16 int32 int64
	// uint uint8 uint16 uint32 uint64 uintptr
	// byte - alias for uint8
	// rune - alias for int32
	// float - float64
	// complex64 complex128

	//Using var
	// var age int = 37
	// Shorthand
	age := 37

	// Constant
	const isCool = true
	// isCool = false

	size, email := 1.3, "borjag90dev@gmail.com"

	fmt.Println(name, age, isCool, email)
	fmt.Printf("%T\n", size)

}

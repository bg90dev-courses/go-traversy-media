<div align="center">

# Go Crash course de Traversy Media

[![Project](https://img.shields.io/badge/Project-Course-yellow.svg)][repo-link]
[![Repository](https://img.shields.io/badge/gitlab-purple?logo=gitlab)][repo-link]
[![Language](https://img.shields.io/badge/Go-00ACD7?logo=go&logoColor=fff)][golang-link]

Curso de GoLang de Traversy Media
https://www.youtube.com/watch?v=SqrbIlUwR0U

</div>
<hr>

## Built with

### Technologies

[<img src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/go/go.png" width=50 alt="Go">][golang-link]

### Platforms

[<img src="https://raw.githubusercontent.com/github/explore/bbd48b997e8d0bef63f676eca4da5e1f76487b56/topics/visual-studio-code/visual-studio-code.png" width=50 alt="Visual Studio Code">][vscode-link]


<div align="center">

## Authors

### **Borja Gete**

[![Mail](https://img.shields.io/badge/borjag90dev@gmail.com-DDDDDD?style=for-the-badge&logo=gmail)][borjag90dev-gmail]
[![Github](https://img.shields.io/badge/BorjaG90-000000.svg?&style=for-the-badge&logo=github&logoColor=white)][borjag90dev-github]
[![Gitlab](https://img.shields.io/badge/BorjaG90-purple.svg?&style=for-the-badge&logo=gitlab)][borjag90dev-gitlab]
[![LinkedIn](https://img.shields.io/badge/borjag90-0077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white)][borjag90dev-linkedin]

</div>

[borjag90dev-gmail]: mailto:borjag90dev@gmail.com
[borjag90dev-github]: https://github.com/BorjaG90
[borjag90dev-gitlab]: https://gitlab.com/BorjaG90
[borjag90dev-linkedin]: https://www.linkedin.com/in/borjag90/
[repo-link]: https://gitlab.com/bg90dev-courses/go-traversy-media
[vscode-link]: https://code.visualstudio.com/docs
[golang-link]: https://golang.org/doc/
